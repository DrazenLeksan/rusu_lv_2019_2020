# -*- coding: utf-8 -*-
"""
Napišite programski kod koji će iscrtati sljedeće slike za mtcars skup podataka:
1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
potrošnju od automobila s automatskim mjenjačem?
4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
mjenjačem. 

@author: leksan
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

mtcars = pd.read_csv('mtcars.csv')

FourCilinders= mtcars[mtcars.cyl == 4]   
SixCilinders= mtcars[mtcars.cyl == 6]
EightCilinders= mtcars[mtcars.cyl == 8]

index1=np.arange(0,len(FourCilinders),1)
index2=np.arange(0,len(SixCilinders),1)  
index3=np.arange(0,len(EightCilinders),1) 

plt.figure()

#1. 1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
plt.bar(index1,FourCilinders.mpg, color=(1,0,0)) 
plt.bar(index2, SixCilinders.mpg, color=(0,1,0))
plt.bar(index3 , EightCilinders.mpg, color=(0,0,1))
plt.title("Potrosnja automobila s 4, 6 i 8 cilindara")
plt.xlabel('auto')
plt.ylabel('mpg')
plt.grid(axis='y',linestyle='--')
plt.legend(['4 cilindra','6 cilindara','8 cilindara'],loc=2) 


#2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s 4, 6 i 8 cilindara.
tezina4=[]
tezina6=[]
tezina8=[]
for i in FourCilinders["wt"]:
    tezina4.append(i)   

for i in SixCilinders["wt"]:
    tezina6.append(i)  

for i in EightCilinders["wt"]:
    tezina8.append(i)   

plt.figure()
plt.boxplot([tezina4, tezina6, tezina8], positions = [4,6,8])  
plt.title("Tezina automobila s 4, 6 i 8 cilindara")
plt.xlabel("Broj klipova")
plt.ylabel("Tezina wt")
plt.grid(axis='y',linestyle='--')

#3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili s ručnim mjenjačem veću
#potrošnju od automobila s automatskim mjenjačem?
automatskiMjenjac=mtcars[(mtcars.am==1)]
autPotrosnja=[]

for mjenjac in automatskiMjenjac.mpg:
    autPotrosnja.append(mjenjac)
    
    
rucniMjenjac=mtcars[(mtcars.am==0)] 
rucnaPotrosnja=[]

for mjenjac in rucniMjenjac.mpg:
    rucnaPotrosnja.append(mjenjac)
    

    
plt.figure()
plt.boxplot([autPotrosnja,rucnaPotrosnja], positions=[0,1])
plt.title("Potrosnja s rucnim i automatskim mjenjacem")
plt.xlabel("Tip mjenjaca")
plt.ylabel("Potrosnja")

#4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim
#mjenjačem. 

qsecAutomatski=[]
hpAutomatski=[]
qsecRucni=[]
hpRucni=[]


for qsec in automatskiMjenjac.qsec:
    qsecAutomatski.append(qsec)
    
    
for hp in automatskiMjenjac.hp:
    hpAutomatski.append(hp)
    
for qsec in rucniMjenjac.qsec:
    qsecRucni.append(qsec)
    
for hp in rucniMjenjac.hp:
    hpRucni.append(hp)
    

plt.figure()
plt.scatter(qsecAutomatski,hpAutomatski,marker='^')
plt.scatter(qsecRucni,hpRucni,edgecolors='g')
plt.title("Odnos ubrzanja i snage automobila s rucnim i automatksim mjenjacem")
plt.xlabel("Ubrzanje")
plt.ylabel("Snaga")
plt.legend(["Automatski mjenjac", "Rucni mjenjac"])




