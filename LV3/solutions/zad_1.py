# -*- coding: utf-8 -*-
"""
Za mtcars skup podataka (nalazi se rusu_lv_2019_20/LV3/resources) napišite programski kod koji će
odgovoriti na sljedeća pitanja:
1. Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort)
2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
3. Kolika je srednja potrošnja automobila sa 6 cilindara?
4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
7. Kolika je masa svakog automobila u kilogramima? 
[]
@author: leksan
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

mtcars=pd.read_csv('mtcars.csv')

#1. Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort)
sortedCars=mtcars.sort_values(by=['mpg'],ascending=True)
print("Pet automobila s najvećom potrošnjom")
print(sortedCars['mpg'].head(5))


#2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
eightCilinders=mtcars[mtcars.cyl==8]
sortedEightCilinders=eightCilinders.sort_values(by=['mpg'], ascending=False)
print("Tri automobila s 8 cilindara i najmanjom potrošnjom")
print(sortedEightCilinders['car'].head(3))


#3. Kolika je srednja potrošnja automobila sa 6 cilindara?
sixCilinders=mtcars[mtcars.cyl==6]
avgMpg=sixCilinders['mpg'].mean()
print("srednja potrošnja automobila sa 6 cilindara")
print(avgMpg)

#4.Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200" lbs?"

fourCilindersMass=mtcars[(mtcars.cyl==4) & (mtcars.wt>=2) & (mtcars.wt<=2.2)]
avgFourCilindersMass=fourCilindersMass['mpg'].mean()
print("srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs")
print(avgFourCilindersMass)


#5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?

brAutomatski=0
brRucni=0
for am in mtcars.am:
   if am==1:
       brAutomatski+=1
   else:
       brRucni+=1
      
print("Broj automobila s automatskim mjenjačem")
print(brAutomatski)
print("Broj automobila s ručnim mjenjačem")
print(brRucni)

#6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?

amAndHundredHp=mtcars[(mtcars.am==1) & (mtcars.hp>100)]
print("Broj automobila s am i 100 hp")
print(amAndHundredHp["car"].count())



#7.  Kolika je masa svakog automobila u kilogramima?

for wt in mtcars.wt:
    kgWt=wt*1000*0.45359237
    print(kgWt)

