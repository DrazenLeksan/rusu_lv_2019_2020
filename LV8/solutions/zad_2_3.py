import numpy as np
import matplotlib as plt
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.metrics import confusion_matrix



train_ds = keras.utils.image_dataset_from_directory(
  "../archive/Train",
  image_size=(48, 48),
  batch_size=64)

test_ds = keras.utils.image_dataset_from_directory(
  "../archive/Test",
  image_size=(48, 48),
  batch_size=64)

train_ds = train_ds.unbatch()
x_train = list(train_ds.map(lambda x, y: x))
y_train = list(train_ds.map(lambda x, y: y))

# Scale images to the [0, 1] range
x_train /= 255
x_test /= 255

print("x_train shape:", x_train.shape)
print(x_train.shape[0], "train samples")
print(x_test.shape[0], "test samples")

# convert class vectors to binary class matrices
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = keras.Sequential(
    [
        keras.Input(shape=input_shape),
        layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Dropout(0.2),
        layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Dropout(0.2),
        layers.Conv2D(128, kernel_size=(3, 3), activation="relu"),
        layers.Conv2D(128, kernel_size=(3, 3), activation="relu"),
        layers.MaxPooling2D(pool_size=(2, 2)),
        layers.Dropout(0.2),
        layers.Flatten(),
        layers.Dense(512, activation="relu"),
        layers.Dropout(0.5),
        layers.Dense(43, activation="softmax"),
    ]
)

model.summary()

batch_size = 64
epochs = 5

model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1)

score = model.evaluate(x_test, y_test, verbose=0)
print("Test loss:", score[0])
print("Test accuracy:", score[1])

p = model.predict(x_test);

