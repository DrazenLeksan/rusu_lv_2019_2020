import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as pltpy
import matplotlib.image as mpimg

imageNew = mpimg.imread('../resources/example_grayscale.png')

try:
    face = sp.face(gray=True)
except AttributeError:
    from scipy import misc
    face = misc.face(gray=True)
    
X = face.reshape((-1, 1)) 
k_means = cluster.KMeans(n_clusters=5,n_init=1)
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
face_compressed = np.choose(labels, values)
face_compressed.shape = face.shape

pltpy.figure(1)
pltpy.imshow(face,  cmap='gray')

pltpy.figure(2)
pltpy.imshow(face_compressed,  cmap='gray')
pltpy.show
