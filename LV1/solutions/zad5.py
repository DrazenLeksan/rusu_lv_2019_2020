
import re
import numpy

numbers=[]

while True:
    imeDatoteke=input("Unesite ime datoteke: ")
    if imeDatoteke == "mbox.txt":
        fhand=open("mbox.txt")
        break
    elif imeDatoteke == "mbox-short.txt":
        fhand=open("mbox-short.txt")
        break
    else:
        print("Nepostojeća datoteka")
        continue


for line in fhand:
    line = line.rstrip()
    if line.startswith('X-DSPAM-Confidence:'):
        extractedNumber=(re.findall('\d+\.\d+', line))
        numbers.append(float(extractedNumber[0]))
        
        

print("Average X-DSPAM-Confidence: ", numpy.mean(numbers))
