#Napišite program koji od korisnika zahtijeva unos brojeva u beskonačnoj petlji sve dok korisnik ne upiše „Done“ (bez
#navodnika). Nakon toga potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
#vrijednost. Osigurajte program od krivog unosa (npr. slovo umjesto brojke) na način da program zanemari taj unos i
#ispiše odgovarajuću poruku. 

br=0
temp=0
tempPolje=[0]*1000



while True:
    try:
        temp=input("Unesi broj")
        if temp=="Done":
            break
        else:
            tempPolje[br]=int(temp)
            br=br+1
    except ValueError:
        print("Niste unijeli broj")
        
    
    
polje=[0]*br
polje=tempPolje
print("Korisnik je unio", br, "brojeva")
print("Maksimalna vrijednost je: ", max(polje))
print("Minimalna vrijednost je: ", min(polje))
print("Srednja vrijednost je: ", sum(polje)/br)   
    
    
