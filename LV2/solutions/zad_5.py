# -*- coding: utf-8 -*-
"""
Created on Sun Nov 21 18:38:03 2021
U direktoriju rusu_lv_2019_20/LV2/resources nalazi se
 datoteka mtcars.csv koja sadrži različita
mjerenja provedena na 32 automobila (modeli 1973-74). 
Prikažite ovisnost potrošnje automobila (mpg) o konjskim
snagama (hp). Na istom grafu prikažite i informaciju o 
težini pojedinog vozila. Ispišite minimalne, maksimalne i
srednje vrijednosti potrošnje automobila. 
@author: leksan
"""

import pandas as pd
import matplotlib.pyplot as plt

fig=plt.figure()
ax=fig.add_subplot(1,1,1)

mtcars=pd.read_csv("mtcars.csv")

plt.scatter(mtcars.mpg, mtcars.hp)

plt.xlabel("mpg")
plt.ylabel("hp")
plt.grid(linestyle='-')

for i in range(0,len(mtcars.mpg)):
    ax.annotate('%.2f' % mtcars.wt[i],      #komentiranje na grafu, podatak koji će biti prikazan na grafu, tipa .2f, za pojedino vozilo
                xy=(mtcars.mpg[i], mtcars.hp[i]), #na koji podatak se odnosi (uzima vrijednosti sa x i y osi za pojedino vozilo)
                xytext=(mtcars.mpg[i]+5, mtcars.hp[i]+40), #na kojoj će se lokaciji prikazati tekst (vrijednost mpg pomaknuta za 2,...)
                arrowprops=dict(arrowstyle="-", facecolor='black'), #povezivanje teksta i točke sa linijom (strelica stil "--" crne boje
                )