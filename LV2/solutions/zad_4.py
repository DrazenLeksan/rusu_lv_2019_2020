

# -*- coding: utf-8 -*-
"""

Simulirajte 100 bacanja igraće kocke (kocka s brojevima 1 do 6). 
Pomoću histograma prikažite rezultat ovih bacanja.
@author: leksan
"""

import numpy 
import matplotlib.pyplot as plt


polje=numpy.random.randint(1,8,size=100)
print(polje)
bins=range(min(polje), max(polje)+1)
print(bins)
plt.hist(polje,bins)
