# -*- coding: utf-8 -*-
"""
Napravite jedan vektor proizvoljne dužine koji sadrži slučajno generirane cjelobrojne vrijednosti 0 ili 1. Neka 1
označava mušku osobu, a 0 žensku osobu. Napravite drugi vektor koji sadrži visine osoba koje se dobiju uzorkovanjem
odgovarajuće distribucije. U slučaju muških osoba neka je to Gaussova distribucija sa srednjom vrijednošću 180 cm i
standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova distribucija sa srednjom vrijednošću 167 cm
i standardnom devijacijom 7 cm. Prikažite podatke te ih obojite plavom (1) odnosno crvenom bojom (0). Napišite
funkciju koja računa srednju vrijednost visine za muškarce odnosno žene (probajte izbjeći for petlju pomoću funkcije
np.dot). Prikažite i dobivene srednje vrijednosti na grafu. 

"""

import numpy as np
import matplotlib.pyplot as plt

polje=[]
uzorkovano_polje=[]

for i in range(0,100):
    polje.append(np.random.randint(0, 2))
print(polje)


plot_m=[]
plot_z=[]



for i in range(0,100):
    if polje[i]==1:
        plot_m.append(np.random.normal(180,7,None))
        
        
        
    else:
        plot_z.append(np.random.normal(167,7,None))
        
        
          

        
plt.hist([plot_z,plot_m], color=["green","red"]);

avg_m=np.average(plot_m)
avg_z=np.average(plot_z)

plt.axvline(avg_m, linestyle=':', linewidth=3)  
plt.axvline(avg_z,  linestyle='--', linewidth=3)      


