# -*- coding: utf-8 -*-
"""


@author: leksan
"""
import cv2 as cv 


img = cv.imread('tiger.png') #load rgb image
hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV) #convert it to hsv

for x in range(0, len(hsv)):
    for y in range(0, len(hsv[0])):
        hsv[x, y][2] += 60

img = cv.cvtColor(hsv, cv.COLOR_HSV2BGR)
cv.imwrite("tiger1.png", img)