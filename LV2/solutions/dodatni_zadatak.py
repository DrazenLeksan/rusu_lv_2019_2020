# -*- coding: utf-8 -*-
"""
Simulirajte bacanje igraće kocke 30 puta. 
Izračunajte srednju vrijednost rezultata i 
standardnu devijaciju. Pokus
ponovite 1000 puta. 
Prikažite razdiobu dobivenih srednjih vrijednosti. 
Što primjećujete? Kolika je srednja vrijednost i
standardna devijacija dobivene razdiobe? 
Što se događa s ovom razdiobom ako pokus 
ponovite 10000 puta? 
@author: leksan
"""

import numpy as np
import matplotlib.pyplot as plt

srednja_vrij=[]
st_dev=[]


for i in range (0,10000):
    niz1=np.random.randint(1,7,size=30)
    srednja_vrij.append(np.average(niz1))
    st_dev.append(np.std(niz1))
    
    
print(st_dev)
    
plt.hist(srednja_vrij)
plt.hist(st_dev)

uk_sr_vrij=np.average(srednja_vrij)
uk_st_dev=np.std(srednja_vrij)

print(uk_sr_vrij)
print(uk_st_dev)



